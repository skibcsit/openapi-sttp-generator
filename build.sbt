name := "openapi-generator-code"

version := "0.1"

scalaVersion := "2.13.4"

// https://mvnrepository.com/artifact/io.swagger.parser.v3/swagger-parser
libraryDependencies += "io.swagger.parser.v3" % "swagger-parser" % "2.0.23"

// https://mvnrepository.com/artifact/com.eed3si9n/treehugger
libraryDependencies += "com.eed3si9n" %% "treehugger" % "0.4.4"

// https://mvnrepository.com/artifact/com.typesafe.scala-logging/scala-logging
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

// https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

// https://mvnrepository.com/artifact/commons-cli/commons-cli
libraryDependencies += "commons-cli" % "commons-cli" % "1.4"
